sap.ui.define([
	"sap/grc/acs/fraud/alertworklist/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/grc/acs/fraud/alertworklist/controller/SectionNavigator",
	"sap/ui/comp/navpopover/LinkData"
], function (BaseController, JSONModel, SectionNavigator, LinkData) {
	"use strict";

	return sap.ui.controller("sap.grc.acs.fraud.alertworklist.FRA_ALERT_MANExtension.controller.DetectionSectionCustom", {
	setAlertDBKey: function () {
			// setAlertDBKey function is always executed when a new alert detailpage is opened.
			// Abuse this functionality to simulate a patternmatched function
			// patternmatched function cannot be used since a router object is not available anywhere in this controller..
			// timeout is needed for model to load, since this is not loaded yet when setAlertDBKey is executed
			setTimeout(function () {
				this.getView().getModel().setSizeLimit(999);
				var sAlertId = this.getView().getBindingContext().getObject().AlertID;
				var sRespPerson = this.getView().getBindingContext().getObject().ResponsiblePerson;
				if (this.sAlertId !== sAlertId || this.sRespPerson !== sRespPerson) {
					this.sAlertId = sAlertId;
					this.sRespPerson = sRespPerson;
					// Set view model
					var data = {
						persoon: true,
						rechtspersoon: false,
						automatisch: true,
						handmatig: false,
						saveBetrokkene: false,
						respPerson: false
					}
				}
			}, 1000);
		},		
		
onOpenModal: function() {



	
	var oFormatDate = sap.ui.core.format.DateFormat.getDateTimeInstance({
			    pattern: "dd-MM-yyyy"
            }); 

	
	
		var sAlertId = this.getView().getBindingContext().getObject().AlertID;
   if (!this._oHelloModal) {
      this._oHelloModal = sap.ui.xmlfragment(this.getView().getId(), "sap.grc.acs.fraud.alertworklist.FRA_ALERT_MANExtension.view.fragment.modal", this);
      this.getView().addDependent(this._oHelloModal);
   }

   // haal de data op van de OData-service en voeg deze toe aan de modale dialoog
  
  	var sMethodenPath = "AlleMethodenTonen>/DetectieRegelSet";
					var oTable = this.getView().byId("tableMethode");
					var aFilters = [],
						oFilter;
					oFilter = new sap.ui.model.Filter("alert_id", sap.ui.model.FilterOperator.EQ, sAlertId);
					aFilters.push(oFilter);

					var oTemplate = new sap.m.ColumnListItem({
						cells: [
							new sap.m.Text({
								text: "{AlleMethodenTonen>alert_id}"
														}),
							new sap.m.Text({
								text: "{AlleMethodenTonen>STRATEGIE}"
							}),
							 new sap.m.Text({
							text: { path: "AlleMethodenTonen>DATUM",
							formatter: function(sValue) {
							return oFormatDate.format(new Date(sValue));
							
								
							}}}),
								new sap.m.Text({
								text: "{AlleMethodenTonen>ONDERZOEK_REDEN}"
						
						
					}),					new sap.m.Text({
								text: "{AlleMethodenTonen>METHOD_ID}"
							
						
					}),					new sap.m.Text({
								text: "{AlleMethodenTonen>METHODE_NAAM}"
							
						
					}),					new sap.m.Text({
								text: "{AlleMethodenTonen>DETECTIE_RISICO_SCORE}"
							}),
						
					
									new sap.m.Text({
								text: "{AlleMethodenTonen>ADDINFO_TEXT}"
							}),
							
									new sap.m.Text({
								text: "{AlleMethodenTonen>VOLGORDE_TEXT}"
							})
						]
					});

					oTable.bindItems({
						path: sMethodenPath,
						filters: aFilters,
						template: oTemplate,
						templateShareable: false
					});

   this._oHelloModal.open();
},

		onCloseModal: function() {
			this._oHelloModal.close();
		},
		onInit: function () {
			   var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZFRA_CDS_MAX_ALLE_REGELS_CDS/", true);
               this._oModel = oModel;

			this._oDetectionSectionDetailDialog = null;
			this._oStrategyExecution = null;
			this._aStrategyExecutions = null;

			//This model is used on the detection section. Collapse and Expand is much faster without ODATA calls
			this._oDetectionModel = new JSONModel({
				DecideButtonEnabled: false
			});
			this._oDetectionModel.setSizeLimit(99999); //increase SizeLimit to enable list binding to show all items
			this.setModel(this._oDetectionModel, "myModel");

			this._oItemTable = this.byId("AlertItems");


			// Add button to AlertItems table toolbar EXTENSION
			var oShowAllDetectionMethodsButton = new sap.m.Button({
				// id: "showAllDetectionMethods",
				text: "Alle opsporingsmethoden tonen",
				press: this.onOpenModal.bind(this)
			});
			this._oItemTable.getHeaderToolbar().addContent(oShowAllDetectionMethodsButton);
		},

		_onExpandSuccess: function () {
			var oModel = this.getView().getModel();
			// this._buildTreeData();
			var yourDOCTYPE = "<!DOCTYPE html..."; // your doctype declaration
			var myWindow = window.open('', 'print_preview');
			var myWindowDoc = myWindow.document;

			myWindowDoc.open();
			myWindowDoc.write(yourDOCTYPE +
				"<head>" +
				"<script id='sap-ui-bootstrap'src='../ui5_ui5/ui2/ushell/resources/sap-ui-core.js' data-sap-ui-libs='sap.m'data-sap-ui-theme='sap_belize'> </script>" +
				"<script>sap.ui.getCore().attachInit(function () {});</script>" +
				"</head><body></body>");

			sap.ui.view({
				viewName: "sap.grc.acs.fraud.alertworklist.FRA_ALERT_MANExtension.view.AllDetectionMethods",
				// controller: "my.own.controller",
				type: sap.ui.core.mvc.ViewType.XML,
				async: true
			}).loaded().then(function (oView) {
				// the instance is available in the callback function
				oView.setModel(oModel);
				oView.setModel(this.getView().getModel("i18n"), "i18n");
				oView.getContent()[0].getPages()[0].bindElement({
					path: "/FRA_CV_StrategyExecution(guid'000d3aaa-7db4-1eeb-9ca0-9e264074b894')",
					parameters: {
						expand: "to_MethodExecution,to_MethodExecution/to_ParameterValues,to_MethodExecution/to_EvaluationText"
					},
				});
				oView.placeAt(myWindowDoc.body)
			}.bind(this));
		},

		_buildTreeData: function () { // EXTENSION
			var oModel = this.getView().getModel();
			var oMyModel = this.getView().getModel("myModel");

			var oData;

			var aItems = oMyModel.getProperty("/Items");
			for (var i = 0; i < aItems.length; i++) {
				var sStrategyExecutionPath = aItems[i].Path;
				var oStrategyExecutionData = oModel.getProperty(sStrategyExecutionPath);
				// 				oStrategyExecutionData.children = [];
				// 				oData = oStrategyExecutionData;
				// var oTable = new sap.m.Table({
				// 	items: {
				// 		// 					path: sStrategyExecutionPath,
				// 		path: "to_MethodExecution",
				// 		template: new sap.m.ColumnListItem({
				// 			cells: [
				// 				new sap.m.Text({
				// 					text: "{Name}"
				// 				}),
				// 				new sap.m.HBox({
				// 					items: [
				// 						new sap.m.Label({
				// 							text: "{i18n>riskScore1}",
				// 							labelFor: "RiskScoreNumber"
				// 						}),
				// 						new sap.m.ObjectNumber({
				// 							number: "{ parts:[{path:'RiskValue'},{path:'Currency'}], type:                         'sap.ui.model.type.Currency', formatOptions: {showMeasure: false} }",
				// 							textAlign: "End",
				// 							unit: "{Currency}",
				// 							emphasized: false
				// 						})
				// 					]
				// 				}),
				// 				new sap.m.Text({
				// 					text: "{myModel>StrategyID}"
				// 				})
				// 			]
				// 		})
				// 	},
				// 	headerToolbar: [
				// 		new sap.m.Toolbar({
				// 			content: [
				// 				new sap.m.Title({
				// 					text: i + "." + oStrategyExecutionData.StrategyDescription
				// 				})
				// 			]
				// 		})
				// 	],
				// 	columns: [
				// 		new sap.m.Column({
				// 			header: [
				// 				new sap.m.Label({
				// 					text: "Methods"
				// 				})
				// 			]
				// 		}), new sap.m.Column({
				// 			header: [
				// 				new sap.m.Label({
				// 					text: "Results"
				// 				})
				// 			]
				// 		}), new sap.m.Column({
				// 			header: [
				// 				new sap.m.Label({
				// 					text: "Evaluation Texts"
				// 				})
				// 			]
				// 		})
				// 	]
				// });

				oTable.bindElement({
					path: sStrategyExecutionPath,
					templateShareable: true
				});

				// this._oDetectionSectionAllDialog.addContent(oTable);

				var aMethodExecutionPaths = oModel.getProperty(sStrategyExecutionPath).to_MethodExecution.__list;
				for (var ii = 0; ii < aMethodExecutionPaths.length; ii++) {
					var oMethodExecutionData = oModel.getProperty("/" + aMethodExecutionPaths[ii]);
					// 					oMethodExecutionData.children = [];
					// 					oData.children.push(oMethodExecutionData);

					var aEvaluationTextPaths = oModel.getProperty("/" + aMethodExecutionPaths[ii]).to_EvaluationText.__list;
					for (var iii = 0; iii < aEvaluationTextPaths.length; iii++) {
						var oEvaluationTextData = oModel.getProperty("/" + aEvaluationTextPaths[iii]);
						// 						oMethodExecutionData.children.push(oEvaluationTextData);
					}
				}
			}

			// 			var data = {root: oData};
			// 			oMyModel.setProperty("/TreeData", data);

		},

		_closeAllDetails: function () {
			this._oDetectionSectionAllDialog.close();
		},

		setAlertDBKey: function (sDBKey) {
			jQuery.sap.log.debug("Section Detection - setAlertDBKey triggered");

			var sObjectPath = this.getModel().createKey("/FRA_CV_AlertRoot", {
				DBKey: sDBKey
			});
			//use a read instead
			this.getModel().read(sObjectPath, {
				urlParameters: ["$expand=to_StrategyExecution,to_AlertItemDetectionSect"],
				success: this._onReadSuccess.bind(this)
			});

			this._clearFilters();
		},

		_showDetails: function (oEvent) {
			//Binding context from local JSON model. The JSON model has a path to the ODATA model
			var oBindingContext = oEvent.getSource().getBindingContext("myModel");
			var oLine = oBindingContext.getObject();
			this._oStrategyExecution = oLine;

			if (!this._oDetectionSectionDetailDialog) {
				this._oDetectionSectionDetailDialog = sap.ui.xmlfragment(this.getView().getId(),
					"sap.grc.acs.fraud.alertworklist.view.fragment.DetectionSectionDetails",
					this);
				// Don't use addDependent here -> Binding on the Dailog is relativ and would create ODATA calls that will fail&nbsp;
				// e.g. FRA_CV_AlertRoot/to_MethodExecution, because the binding context of the parent view points to AlertRoot
				// this.getView().addDependent(this._oDetectionSectionDetailDialog);
			}

			//Set the correct path
			this._oDetectionSectionDetailDialog.bindElement({
				path: oLine.Path, //path to ODATA model
				parameters: {
					expand: "to_MethodExecution,to_MethodExecution/to_ParameterValues,to_MethodExecution/to_EvaluationText"
				},
				events: {
					dataRequested: function () {
						this._oDetectionSectionDetailDialog.setBusy(true);
					}.bind(this),
					dataReceived: function () {
						this._oDetectionSectionDetailDialog.setBusy(false);
					}.bind(this)
				}
			});
			//Because addDependent wasn't used the model must be set explicitly
			if (!this._oDetectionSectionDetailDialog.getModel()) {
				this._oDetectionSectionDetailDialog.setModel(this.getView().getModel());
				this._oDetectionSectionDetailDialog.setModel(this.getView().getModel("i18n"), "i18n");
			}
			this._oDetectionSectionDetailDialog.open();
		},

		_closeDetails: function () {
			this._oDetectionSectionDetailDialog.close();

			var Path = this._oStrategyExecution.Path.substring(1, this._oStrategyExecution.Path.length);
			/* eslint-disable sap-no-ui5-prop-warning */
			var object = this.getView().getModel().oData[Path];
			// The dialog "Detection Section Details" executes an expand to "to_MethodExecution". Unfortunately also an OData get entity is executed for this single
			// strategy execution. The OData call does not return the content for the fields Latest, DetectionMethod, VisibleCollapse, VisibleExpand and StrategyKeyString.
			// An ABAP implementation is not possible because the Visible flags depend on ALL strategy executions and could have been changed on the UI.
			// Only solution so far: Change the ODATA content directly.
			object.Latest = this._oStrategyExecution.Latest;
			object.DetectionMethod = this._oStrategyExecution.DetectionMethod;
			object.VisibleCollapse = this._oStrategyExecution.VisibleCollapse;
			object.VisibleExpand = this._oStrategyExecution.VisibleExpand;
			object.StrategyKeyString = this._oStrategyExecution.StrategyKeyString;
			this.getView().getModel().oData[Path] = object;
			/* eslint-enable sap-no-ui5-prop-warning */
		},

		_buildLocalJSONModel: function (aStrategyExecution) {
			var oLine = null;
			var aData = [];
			var aRiskScore = [];
			var aRiskValue = [];
			var aThreshold = [];
			var aLifecycleStatus = [];
			var iItemNumber = 0;
			var iNumber = 0;
			var i = 0;

			var fnPushUnique = function (aArray, oObject) {
				if (aArray.indexOf(oObject) === -1) {
					aArray.push(oObject);
				}
			};

			for (i in aStrategyExecution) {
				if (aStrategyExecution.hasOwnProperty(i)) {
					oLine = aStrategyExecution[i];
					aData.push({
						AlertItemNumber: oLine.AlertItemNumber,
						AlertItemReadOnly: oLine.AlertItemReadOnly,
						Currency: oLine.Currency,
						Description: oLine.Description,
						DetectionObjectType: oLine.DetectionObjectType,
						ExecutionTime: oLine.ExecutionTime,
						Latest: oLine.Latest,
						LifecycleStatus: oLine.LifecycleStatusDescription,
						DetectionMethod: oLine.DetectionMethod,
						RiskScore: oLine.RiskScore,
						RiskValue: oLine.RiskValue,
						StrategyDescription: oLine.StrategyDescription,
						StrategyKeyString: oLine.StrategyKeyString,
						Threshold: oLine.Threshold,
						VisibleCollapse: oLine.VisibleCollapse,
						VisibleExpand: oLine.VisibleExpand,
						Path: this.getModel().createKey("/FRA_CV_StrategyExecution", {
							StrategyExecutionKey: oLine.StrategyExecutionKey
						}),
						ItemPath: this.getModel().createKey("/FRA_CV_AlertItemDetectionSect", {
							DBKey: oLine.AlertItemKey
						})
					});

					fnPushUnique(aRiskScore, oLine.RiskScore);
					fnPushUnique(aRiskValue, oLine.RiskValue);
					fnPushUnique(aThreshold, oLine.Threshold);
					fnPushUnique(aLifecycleStatus, oLine.LifecycleStatusDescription);

					if (oLine.AlertItemNumber !== iItemNumber) {
						iNumber++;
						iItemNumber = oLine.AlertItemNumber;
					}
				}
			}

			var oModelData = this._oDetectionModel.getData();
			oModelData.Items = aData;
			oModelData.NumberOfAlerts = iNumber;
			oModelData.RiskScoreFilter = [];
			oModelData.ThresholdFilter = [];
			oModelData.RiskValueFilter = [];
			oModelData.LifecycleStatusFilter = [];

			aRiskScore.forEach(function (oRiskScore) {
				oModelData.RiskScoreFilter.push({
					RiskScore: oRiskScore
				});
			});

			aRiskValue.forEach(function (oRiskValue) {
				oModelData.RiskValueFilter.push({
					RiskValue: oRiskValue,
					Currency: oLine.Currency
				});
			});

			aThreshold.forEach(function (oThreshold) {
				oModelData.ThresholdFilter.push({
					Threshold: oThreshold
				});
			});

			aLifecycleStatus.forEach(function (oLifecycleStatus) {
				oModelData.LifecycleStatusFilter.push({
					LifecycleStatus: oLifecycleStatus
				});
			});

			this._oDetectionModel.setData(oModelData);
		},

		_removeExpansionStateFromItems: function () {
			var aItems = this._oDetectionModel.getProperty("/Items");
			aItems.forEach(function (oItem) {
				oItem.VisibleCollapse = false;
				oItem.VisibleExpand = false;
			});
		},

		_getFilterbarFilter: function () {
			var aFilters = [];

			var fnPushFilter = function (sFilterId) {
				var oFilter = this._getFilter(sFilterId);
				if (oFilter) {
					aFilters.push(oFilter);
				}
			}.bind(this);

			fnPushFilter("RiskValue");
			fnPushFilter("RiskScore");
			fnPushFilter("Threshold");
			fnPushFilter("LifecycleStatus");

			if (aFilters.length === 1) {
				return aFilters[0];
			}

			if (aFilters.length === 0) {
				return null;
			}

			return new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			});
		},

		_getFilter: function (sFilterId) {
			var oFilterCombobox = this.byId(sFilterId + "FilterCB");
			var aSelectedKeys = oFilterCombobox.getSelectedKeys();
			var aFilters = [];
			aSelectedKeys.forEach(function (sKey) {
				aFilters.push(new sap.ui.model.Filter(sFilterId, sap.ui.model.FilterOperator.EQ, sKey));
			});

			if (aFilters.length === 0) {
				return null;
			}

			if (aFilters.length === 1) {
				return aFilters[0];
			}

			return new sap.ui.model.Filter({
				filters: aFilters,
				and: false
			});
		},

		_clearFilters: function () {
			var fnClearFilter = function (sFilterId) {
				this.byId(sFilterId + "FilterCB").clearSelection();
			}.bind(this);

			fnClearFilter("RiskValue");
			fnClearFilter("RiskScore");
			fnClearFilter("Threshold");
			fnClearFilter("LifecycleStatus");
		},

		_formatID: function (sID1, sID2, sID3) {
			var sText;
			if (sID1 === undefined && sID2 === undefined && sID3 === undefined) { // EXTENSION
				// Do nothing
			} else {
				if (sID3 === undefined) {
					if (sID2 === undefined) {
						sText = this.getModel("i18n").getResourceBundle().getText("IDWithBrackets", [sID1]);
					} else {
						sText = this.getModel("i18n").getResourceBundle().getText("NameIDWithBrackets", [sID1, sID2]);
					}
				} else {
					sText = this.getModel("i18n").getResourceBundle().getText("IDandVersionWithBrackets", [sID1, sID2, sID3]);
				}
			}
			return sText;
		},

		_formatText: function (sText) {
			return "<div>" + sText + "</div>";
		},

		_filterStrategies: function (oEvent) {
			var i;
			//Get the current line
			var oBindingContext = oEvent.getSource().getBindingContext("myModel");
			var oLine = oBindingContext.getObject();
			var bExpand = oLine.VisibleExpand;

			//Switch from collapse to expand and vice versa
			this._oDetectionModel.setProperty(oBindingContext.getPath() + "/VisibleExpand", !bExpand);
			this._oDetectionModel.setProperty(oBindingContext.getPath() + "/VisibleCollapse", bExpand);

			//Set Filter for executing expand and collapse
			var aFilter = [];
			var oBinding = this._oItemTable.getBinding("items");
			var aFilterOld = oBinding.aFilters;
			var sValue = oLine.AlertItemNumber + "_" + oLine.DetectionObjectType;

			var oLatestExecutionFilter = new sap.ui.model.Filter("Latest", "EndsWith", "Yes");
			aFilter.push(oLatestExecutionFilter);

			if (bExpand === true) {
				var oFilter2 = new sap.ui.model.Filter("Latest", "StartsWith", sValue);
				aFilter.push(oFilter2);
				for (i in aFilterOld) {
					//Take over already existing filters (other nodes are already expanded)
					if (aFilterOld[i].oValue1 !== oLatestExecutionFilter.oValue1 && aFilterOld[i].oValue1 !== oFilter2.oValue1) {
						aFilter.push(aFilterOld[i]);
					}
				}
			} else {
				for (i in aFilterOld) {
					//Take over already existing filters (other nodes are already expanded) and collapse the current Alter Item
					if (aFilterOld[i].oValue1 !== oLatestExecutionFilter.oValue1 && aFilterOld[i].oValue1 !== sValue) {
						aFilter.push(aFilterOld[i]);
					}
				}
			}
			oBinding.filter(aFilter, false);
		},

		_onFilterChanged: function () {
			this._applyFilterBarFilters();
		},

		_onUpdateFinished: function () {
			var sCount = this._countVisibleAlertItems();
			var oI18NModel = this.getModel("i18n");
			if (oI18NModel) {
				var sTitle = oI18NModel.getResourceBundle().getText("alertItemsWithCount", [sCount]);
				this._oDetectionModel.setProperty("/title", sTitle);
			}
		},

		_countVisibleAlertItems: function () {
			var oBinding = this._oItemTable.getBinding("items");
			var aCurrentContexts = oBinding.getCurrentContexts();
			var aDistinctAlertItemNumbers = [];
			aCurrentContexts.forEach(function (oContext) {
				var iAlertItemNumber = oContext.getProperty("AlertItemNumber");
				if (aDistinctAlertItemNumbers.indexOf(iAlertItemNumber) === -1) {
					aDistinctAlertItemNumbers.push(iAlertItemNumber);
				}
			});

			return aDistinctAlertItemNumbers.length;
		},

		onDetectionObjectLinkPress: function (oEvent) {
			this._sIdPrefix = this.getView().getId();

			if (!this._oDetectionObjectPopup) {
				this._oDetectionObjectPopup = sap.ui.xmlfragment(this._sIdPrefix,
					"sap.grc.acs.fraud.alertworklist.view.fragment.DetectionObjectTypePopover",
					this);
				this.getView().addDependent(this._oDetectionObjectPopup);
			}
			//Binding context from local JSON model
			var oBindingContext = oEvent.getSource().getBindingContext("myModel");
			var oLine = oBindingContext.getObject();

			this._oDetectionObjectPopup.bindElement({
				path: oLine.ItemPath,
				templateShareable: true
			});
			this._oDetectionObjectPopup.openBy(oEvent.getSource());
		},

		onNavTargetsObtained: function (oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext("myModel");
			var oLine = oBindingContext.getObject();

			var sHref, sHrefStrategy, iPos;
			var oParameters = oEvent.getParameters();
			if ("actions" in oParameters) {
				for (var i in oParameters.actions) {
					if (oParameters.actions.hasOwnProperty(i)) {
						sHref = oParameters.actions[i].getHref();
						//Find the correct navigation target (to strategy with parameters)
						if (sHref.indexOf("DetectionStrategy-manage") !== -1) {
							sHrefStrategy = sHref;
						}
						//Links to related Apps should be called without any parameters
						iPos = sHref.indexOf("?");
						oParameters.actions[i].setHref(sHref.substr(0, iPos));
					}
				}
			}
			oParameters.show(oLine.StrategyDescription, new LinkData({
				text: this.getModel("i18n").getResourceBundle().getText("details"),
				href: sHrefStrategy
			}));
		},

		onBeforePopover: function (oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext("myModel");
			var oLine = oBindingContext.getObject();
			//Set navigation attributes
			oEvent.getParameters().setSemanticAttributes({
				INBOUND_ROOT_KEY: oLine.StrategyKeyString,
				SKIP_INITIAL_SCREEN: "X"
			});
			oEvent.getParameters().open();
		},

		_getExternalNavigationTargetForDetectionMethod: function (sDetectionMethodKey) {
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			return oCrossAppNavigator.hrefForExternal({
				target: {
					semanticObject: "DetectionMethod",
					action: "manageDetectionMethod"
				},
				params: {
					DetectionMethodKey: sDetectionMethodKey
				}
			});
		},

		_onReadSuccess: function (oData) {
			if (oData && oData.to_StrategyExecution && oData.to_StrategyExecution.results) {
				this._buildLocalJSONModel(oData.to_StrategyExecution.results);
				this._applyExpansionFilter();
				this._aStrategyExecutions = oData.to_StrategyExecution.results;
				if (!oData.ReadonlyFlag) {
					this._setDetectionStatusFilter();
				}
			}

			this._setDecideButtonEnableState();
		},

		_setDetectionStatusFilter: function () {
			var sLifecycleStatusInProcess = "02";

			for (var i = 0; i < this._aStrategyExecutions.length; i++) {
				if (this._aStrategyExecutions[i].LifecycleStatus === sLifecycleStatusInProcess) {
					this.byId("LifecycleStatusFilterCB").setSelectedKeys([this._aStrategyExecutions[i].LifecycleStatusDescription]);
					this._applyFilterBarFilters();
					return;
				}
			}
		},

		_applyFilterBarFilters: function () {
			var oFilter = this._getFilterbarFilter();
			if (oFilter) {
				this._removeExpansionStateFromItems();
				this._applyFilter(oFilter);
			} else {
				this._buildLocalJSONModel(this._aStrategyExecutions);
				this._applyExpansionFilter();
			}
		},

		_applyExpansionFilter: function () {
			this._applyFilter(new sap.ui.model.Filter("Latest", "EndsWith", "Yes"));
		},

		_applyFilter: function (oFilter) {
			var oBinding = this._oItemTable.getBinding("items");
			oBinding.filter(oFilter);
			this._oItemTable.removeSelections();
			this._setDecideButtonEnableState();
		},

		onDecideButtonPress: function () {
			var aSelectedContexts = this._oItemTable.getSelectedContexts();
			var aSelectedAlertItemNumbers = [];
			aSelectedContexts.forEach(function (oSelectedContext) {
				var oData = oSelectedContext.getObject();
				if (oData && aSelectedAlertItemNumbers.indexOf(oData.AlertItemNumber) === -1) {
					aSelectedAlertItemNumbers.push(oData.AlertItemNumber);
				}
			});
			SectionNavigator.sendData("DECISION", {
				aSelectedAlertItemNumbers: aSelectedAlertItemNumbers
			});
			SectionNavigator.to("DECISION");
		},

		onTableSelectionChange: function (oEvent) {
			var oParameters = oEvent.getParameters();

			this._setSelectionForAlertItems(oParameters.listItem, oParameters.selected);
			this._deselectReadOnlyAlertItems();
			this._setDecideButtonEnableState();
		},

		_setSelectionForAlertItems: function (oListItem, bSelected) {
			var oBindingContext = oListItem ? oListItem.getBindingContext("myModel") : null;
			var sAlertItemNumber = oBindingContext ? oBindingContext.getObject().AlertItemNumber : null;

			var oItems = this._oItemTable.getItems();
			oItems.forEach(function (oItem) {
				var oContext = oItem.getBindingContext("myModel");
				var sCurrentAlertItemNumber = oContext ? oContext.getProperty("AlertItemNumber") : null;
				if (sCurrentAlertItemNumber === sAlertItemNumber) {
					oItem.setSelected(bSelected);
				}
			});
		},

		_deselectReadOnlyAlertItems: function () {
			var aSelectedItems = this._oItemTable.getSelectedItems();
			aSelectedItems.forEach(function (oItem) {
				var oContext = oItem.getBindingContext("myModel");
				var bItemIsReadOnly = oContext ? oContext.getProperty("AlertItemReadOnly") : false;
				if (bItemIsReadOnly) {
					oItem.setSelected(false);
				}
			});
		},

		_setDecideButtonEnableState: function () {
			var bContainsSelection = this._oItemTable.getSelectedItems().length > 0;
			this._oDetectionModel.setProperty("/DecideButtonEnabled", bContainsSelection);
		},

		onEvaluationTextListModelContextChange: function (oEvent) {
			this._bindEvaluationTextListItems(oEvent.getSource());
		},

		_bindEvaluationTextListItems: function (oList) {
			var ADDRESS_SCREENING_METHOD_TYPE = 3;
			var oBindingContext = oList.getBindingContext();
			var oBoundObject = oBindingContext.getObject();
			if (oBoundObject.Type === ADDRESS_SCREENING_METHOD_TYPE) {
				oList.setGrowingThreshold(15); //only show 15 rows evaluation text for address screening methods
			} else {
				oList.setGrowingThreshold(10000);
			}
		},

		onExit: function () {
			if (this._oDetectionSectionDetailDialog) {
				this._oDetectionSectionDetailDialog.destroy();
				this._oDetectionSectionDetailDialog = null;
			}
		}
	});
});
